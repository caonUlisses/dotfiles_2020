" ISOLATED CONFIGS

" general
source ~/.vim/plugins/general.vim

" plugins
source ~/.vim/plugins/list.vim

" coc
source ~/.vim/plugins/coc.vim

" keymaps
source ~/.vim/plugins/keymaps.vim

"meta
source ~/.vim/plugins/meta.vim

"nerdtree
source ~/.vim/plugins/nerdtree.vim

" docker
let g:docker_open_browser_cmd = 'open'

" current-word
hi CurrentWordTwins ctermfg=0 ctermbg=255 cterm=bold
hi CurrentWord ctermfg=0 ctermbg=7 cterm=underline,bold

" terraform
let g:terraform_fmt_on_save=1

" brackets
let g:rainbow_active = 0

" messages
let g:git_messenger_close_on_cursor_moved=0

" autoformat
noremap <F3> :Autoformat<CR>

" snippets
let g:UltiSnipsExpandTrigger = "<nop>"

" sql
source ~/.vim/plugins/db.vim

" fzf
function! s:find_git_root()
  return system('git rev-parse --show-toplevel 2> /dev/null')[:-2]
endfunction

command! ProjectFiles execute 'Files' s:find_git_root()
