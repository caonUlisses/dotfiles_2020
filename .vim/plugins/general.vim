" General Settings
filetype plugin on
set encoding=utf8    " enables UFT-8
set autowriteall     " autosave file if vim exits
set relativenumber   " set the line number indicator as relative to line is currently in
set copyindent       " copy with indentation
syntax on            " syntax highlight
let mapleader = ","  " set the <Leader> key as ,

" Nice tabs
set guioptions-=e

" Spacing
set expandtab             " turn tabs into spaces
set tabstop=4             " each tab will have 2 spaces
set shiftwidth=4          " identation for 2
set number                " show numbers
set smartindent           " make identation work
set clipboard=unnamed     " fix clipboard identation
set autoindent            " set autoindent for newlines
set si
filetype plugin indent on " fix autoindent based on filetype

autocmd FileType json syntax match Comment +\/\/.\+$+
