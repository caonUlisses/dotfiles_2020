" Source vimrc file on save
augroup autosourcing
  autocmd!
  autocmd BufWritePost .vimrc source %
augroup END

nmap <Leader>ev :tabedit $MYVIMRC<cr>
