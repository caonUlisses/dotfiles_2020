" Keymaps
nnoremap <leader><tab> :NERDTreeToggle<CR>
nnoremap <leader><tab>f :NERDTreeFind<CR>
nnoremap <leader>gs :Magit<cr>
nnoremap <Leader>gb :Gblame<CR>  " git blame
nmap <Leader>vpi :PlugInstall<cr>
nmap <Leader>vpu :PlugUpdate<cr>
nmap <Leader>vpc :PlugClean<cr> y <cr>
nmap <c-F> :Ag<cr>
nmap <c-p> :ProjectFiles<cr>

noremap <Leader><C-J> <C-W><C-J> :q <cr>
noremap <Leader><C-H> <C-W><C-H> :q <cr>
noremap <Leader><C-L> <C-W><C-L> :q <cr>
noremap <Leader><C-K> <C-W><C-K> :q <cr>
nnoremap ; :

nnoremap gn :tabnew<cr>

map /  <Plug>(incsearch-forward)
map ?  <Plug>(incsearch-backward)
map g/ <Plug>(incsearch-stay)
