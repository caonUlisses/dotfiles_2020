call plug#begin('~/.vim/plugged')

" autocomplete
Plug 'neoclide/coc.nvim', {'tag': '*', 'branch': 'release'}

" editing
" Plug 'mattn/emmet-vim'
Plug 'AndrewRadev/tagalong.vim'
Plug 'Chiel92/vim-autoformat'
Plug 'alvan/vim-closetag'
Plug 'cohama/lexima.vim'
Plug 'frazrepo/vim-rainbow'
Plug 'terryma/vim-multiple-cursors'
Plug 'tpope/vim-surround'
Plug 'junegunn/goyo.vim'
Plug 'haya14busa/incsearch.vim'
Plug 'tpope/vim-commentary'
Plug 'AndrewRadev/splitjoin.vim'
Plug 'gregsexton/MatchTag'

" project
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-vinegar'
Plug 'francoiscabrol/ranger.vim'
Plug 'preservim/nerdtree'
Plug 'rhysd/git-messenger.vim'

" docker
Plug 'skanehira/docker.vim'
Plug 'ekalinin/Dockerfile.vim'

" status line
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" snippets
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

" git
Plug 'airblade/vim-gitgutter'
Plug 'jreybert/vimagit'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rhubarb'

" php
Plug 'dominikduda/vim_current_word'
Plug 'adoy/vim-php-refactoring-toolbox'
Plug 'noahfrederick/vim-composer'

" terraform
Plug 'hashivim/vim-terraform'

" style
Plug 'ryanoasis/vim-devicons'

" sql
Plug 'tpope/vim-dadbod'
Plug 'joereynolds/SQHell.vim'

" random
Plug 'mhinz/vim-startify'
" Plug 'bagrat/vim-buffet'
Plug 'airblade/vim-rooter'

call plug#end()
